# Reconnaissance faciale avec  OpenCV 3.4.0

## Les exigences se trouvent dans le fichier exigences.txt

1> create_database.py (utilisé pour créer une base de données sqlite)

2> record_face.py (enregistre le nom associé au visage)

3> trainer.py (utilisé pour entraîner LBPHFaceRecognizer pour la reconnaissance faciale)

4> détecteur.py (détecte le visage à partir de données préalablement formées et récupère les informations correspondantes de la base de données)